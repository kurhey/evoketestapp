import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import YouTube from "react-youtube";
import { loadavg } from 'os'

export class Youtube extends React.Component<RouteComponentProps<{}>, { videos: any, loading: boolean }> {

    constructor(props: any) {
        super(props);
        this.state = { videos: [], loading: true };
    }
    

  componentDidMount() {
    var that = this;
    var API_key = "AIzaSyBnxoPjPyjO_gDMj_3ybUN6pJcaiayghjI";
    var channelID = "UC6yW44UGJJBvYTlfC7CRg2Q";
    var maxResults = 10;
    var url =
      "https://www.googleapis.com/youtube/v3/search?key=" +
      API_key +
      "&channelId=" +
      channelID +
      "&part=snippet,id&order=date&maxResults=" +
      maxResults;

    fetch(url)
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(function(data) {
        that.setState({ videos: data.items, loading: false });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {

    if (this.state.loading) {
      return null;
    }

    return (
      <div className="App">
        <YouTube
          videoId={this.state.videos[1].id.videoId}
          opts={{
            height: "390",
            width: "640",
            playerVars: {
              autoplay: 1
            }
          }}
        />
      </div>
    );
  }

}

