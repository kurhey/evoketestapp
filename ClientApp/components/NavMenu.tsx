import * as React from 'react';
import { Link, NavLink } from 'react-router-dom';

export class NavMenu extends React.Component<{}, {}> {
    public render() {
        return <div className='main-nav'>
                <div className='navbar navbar-inverse'>
                <div className='navbar-header'>
                    <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                        <span className='sr-only'>Toggle navigation</span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                    </button>
                    <Link className='navbar-brand' to={ '/Instagram' }>EvokeTest</Link>
                </div>
                <div className='clearfix'></div>
                <div className='navbar-collapse collapse'>
                    <ul className='nav navbar-nav'>
                        <li>
                            <NavLink to={ '/instagram' } exact activeClassName='active'>
                                <span className='glyphicon glyphicon-home'></span> Instagram
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={ '/youtube' } activeClassName='active'>
                                <span className='glyphicon glyphicon-education'></span> Youtube
                            </NavLink>
                        </li>                   
                    </ul>
                </div>
            </div>
        </div>;
    }
}
