import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Youtube } from './components/Youtube';
import { Instagram } from './components/Instagram';

export const routes = <Layout>
    <Route exact path='/youtube' component={ Youtube } />
    <Route path='/instagram' component={ Instagram } />
</Layout>;
