import * as React from 'react';
import * as request from 'superagent';
import { RouteComponentProps } from 'react-router';
import 'isomorphic-fetch';




export class Instagram extends React.Component<RouteComponentProps<{}>, { photos: any, slideCount: number }> {

    constructor(props: any) {
        super(props);
        this.state = { photos: [], slideCount: 0 }
        this.nextImage = this.nextImage.bind(this);
        this.previousImage = this.previousImage.bind(this); 
    }

    fetchPhotos(): void {
        fetch('api/ApiData/InstagramAccessToken')
            .then(response => response.text())
            .then(data => {
                request
                    .get(`https://api.instagram.com/v1/users/self/media/recent/?access_token=${data}`)
                    .then((res: any) => {
                        this.setState({
                            photos: res.body.data
                        })
                    })
            });
        
    }

    componentWillMount() {
        this.fetchPhotos();
    }

    BackArrow = (props: any) => (
        <div onClick={props.previousImage} style={{ fontSize: '2em', marginRight: '12px' }}>
            <i className="fa fa-angle-left fa-2x" aria-hidden="true">LEFT</i>
        </div>
    )

    NextArrow = (props: any) => (
        <div onClick={props.nextImage} style={{ fontSize: '2em', marginLeft: '12px' }}>
            <p><i className="fa fa-angle-right fa-2x" aria-hidden="true">RIGHT</i></p>
        </div>
    )

    nextImage() {
        this.setState({ slideCount: this.state.slideCount + 1 })
    }

    previousImage() {
        this.setState({ slideCount: this.state.slideCount - 1 })
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">Welcome to My Instagram</h1>
                </header>
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '30px' }}>
                    {this.state.slideCount !== 0 ? <this.BackArrow previousImage={this.previousImage} /> : ''}
                    {this.state.photos.map((photo: any, key: any) => {
                        if (this.state.photos.indexOf(photo) === this.state.slideCount) {
                            return (
                                <div key={photo.id} style={{ margin: '0 auto' }}>
                                    <img src={photo.images.standard_resolution.url} alt='' />
                                    <div style={{ width: '600px', margin: '24px auto', fontStyle: 'italic' }}>
                                    </div>
                                </div>
                            )
                        }
                        return ''
                    })}
                    {this.state.slideCount !== (this.state.photos.length - 1) ? <this.NextArrow nextImage={this.nextImage} /> : ''}
                </div>
            </div>


        );
    }
}

    
    


